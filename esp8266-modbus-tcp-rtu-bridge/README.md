# ESP8266 Modbus TCP-RTU Bridge

Dieser Adapter wandelt das Modbus TCP Protokoll in ein RTU Protokoll. Damit lässt sich die Therma V über WLAN vom heimischen Netzwerk aus ansprechen.
Erstaunlicherweise kann sich der Adapter problemlos mit dem Heimischen Netzwerk verbinden, auch wenn sich dieser innerhalb der Therma V befindet.

<img src="Aufbau.jpg" width="20%">

Für den Aufbau wird ein ESP8266-01, ein MAX485 Konverter und ein AMS1117 5V zu 3.3V Step-Down Wandler benötigt. Als Gehäuse kann z.B. eine mini Aufputz Verteilerdose aus dem Baumarkt herhalten.
Für die Spannungsversorgung kann ein USB Netzteil verwendet werden.
Alternativ ließe sich auch ein 12V zu 3.3V Step-Down Wandler verwenden und die 12V z.B. vom Bedienteil oder dem Anschluss für das LG-WLAN Modul abzweigen (ungetestet).

## Verdratung
```
ESP8266-01    MAX485     Power supply
VCC & CH_PD   VCC        3.3V
GND           GND        GND
TX            DI
RX            RO
GRIO2         DE & RE
```
Das USB Netzteil kann z.B. über die Anschlussklemmen L und N Klemmen des Thermostats mit Strom versorgt werden.
Bei der HM091MR.U44 sind das die Klemmen 21 und 22 der Klemmleiste 2.

## Software
Das für den ESP benötigte [Programm](Modbus-tcp-rtu-bridge.ino) muss z.B. per Arduino IDE auf den ESP übertragen werden.
Sobald der ESP mit Strom versorgt wird, spannt dieser ein WLAN Hotspot auf, über das sich das heimische WLAN Netzwerk und WLAN Passwort einstellen lässt. Sollte dieses Netzwerk bei einem zukünftigen Start nicht erreichbar sein, wird wieder der Hotspot errichtet.
Anschließend wartet der ESP auf Modbus anfragen. Sobald die erste Anfrage eingegangen ist, pollt der die angegebene Adresse ein mal pro Minute wenn keine Anfragen mehr eintreffen, und stellt so sicher, dass die Heizung nicht mit dem CH-03 Fehler aus geht.
