# LG Therma V

## Modbus Zugriff
In diesem Projekt beschreibe ich wie auf die LG Therma V Wärmepumpe per Modbus zugegriffen werden kann.
Da es zum einen unterschiedliche Wärmepumpen Modelle gibt, und zum anderen Unterschiedliche Hardware und Software für den Zugriff eingesetzt werden kann, beschreiben die Abschnitte immer nur Teile der Lösung und nicht alle sind für jeden relevant.

### Verbindung
Beim Auslesen gibt es mehrere Probleme. Die Therma V mag es gar nicht, wenn man sie nicht optimal anspricht. Zum einen gibt es das Problem mit dem Abfragen von zu großen Bereichen, zum anderen ist auch das Timing beim Pollen sehr wichtig. Weder ein zu schnelles noch zu langsames Pollen funktioniert über eine längere Zeit fehlerfrei.

#### Verbindungsaufbau
Die Therma V reagiert erst ab der Dritten Anfrage. Hierbei ist zu beachten, dass diese drei Anfragen nicht an beliebige Register gestellt werden dürfen. Daher sollte zuerst dreimal ein Input oder Coil Register abgefragt werden.

#### Verbindungsende
Die Therma V stützt ab, wenn länger als 5 Minuten keine Abfragen erfolgen. Auf dem Bedienteil wird dann der Fehler CH 03 angezeigt. Dann hilft nur ein Trennen von der Stromversorgung, damit die Heizung wieder ohne Modbus Polling funktioniert. Es muss also sichergestellt werden, dass nie mehr mit dem Pollen der Register aufgehört wird, nachdem damit angefangen wurde. 
Laut LG Support ist dies ein "Feature", damit man mitbekommt, falls es zu verbindungsproblemen mit dem Modbus kommt. Sollte also einmal das System ausfallen, mit dem ich die Therma V überwache, bekomme ich dies auf jedenfall mit, da es dann im Haus kalt wird ;)

## Hardware
Die Verbindung zur Therma V kann mit unterschiedlicher Hardware erfolgen.

### USB-Stick
Die günstigste und einfachste Lösung stellt ein USB-485 Converter Adapter dar (z.B. https://www.amazon.de/gp/product/B07K3V381Z oder https://www.waveshare.com/usb-to-rs485.htm). Dieser wird einfach über zwei dräte (bis zu 1000m Länge) mit der Therma V verbunden und in den PC gesteckt.
Nachteil dieser Variante ist, dass hierdurch eine elektrische Verbindung zwischen der Therma V und dem PC besteht. Ein Fehler in der Wärmepumpe kann also zue Beschädigung des PCs führen.

### Netzwerk
Das Modbus Protokoll kann auch über ein Netzwerk verwendet werden. Um die Therma V mit dem Netzwerk zu verbinden, wird eine TCP-RTU Bridge benötigt. Hierfür gibt es fertige Lösungen zu kaufen. Alternativ kann ein solcher Adapter auch selbst gebaut werden.

#### ESP8266 WLAN Adapter
Die Verwendung eines ESP8266 zusammen mit zwei weiteren Bauteinel hat die entscheidenen Vorteile, das diese Lösung sehr günstig ist, und obendrein kann hiermit sichergestellt werden, dass niemals aufgehört wird den Modbus zu pollen, auch wenn der abfragende PC abgeschaltet wird. Die Heizung fällt also nicht aus, wenn es ein Problem mit dem abfragenden System gibt.
Da dieser Adapter nicht fertig gekauft werden kann, sollte man mit einem Lötkolben umgehen können, wenn man nicht dauerhaft einen fliegenden Aufbau verwenden möchte.
Die Materialkosten bewegen sich um die 10€. Eine detaillierte Anleitung ist [hier](esp8266-modbus-tcp-rtu-bridge/README.md) beschrieben.


## Geräte

### HM091MR.U44
Anschlüsse, Dipschalter

## Software

### ioBroker
Die Heimautomatisierungssoftware ioBroker kann verwendet werden om den Status der Therma V auszulesen, oder auch um einige Parameter zu steuern.
[Hier](ioBroker/README.md) ist die Konfiguration beschrieben. 